package com.example.finalproject2;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.TextView;

public class LiftTimer extends WearableActivity {

    TextView timerView;
    TextView liftSetCounter;

    CountDownTimer timer;
    long countdownTime = 0;
    boolean timerRunning;
    int currSetNum = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lift_timer);

        timerView = findViewById(R.id.timerView);
        timerView.setText(Long.toString(countdownTime / 1000));

        liftSetCounter = findViewById(R.id.setCounter);
        liftSetCounter.setText("Current Set #: " + currSetNum);
        // Enables Always-on
        setAmbientEnabled();
    }


    public void onClickStart(View view) {
        if(timerRunning) stopTimer();
        else starTimer();
    }

    private void starTimer() {
        timer = new CountDownTimer(countdownTime, 1000) {

            public void onTick(long millisUntilFinished) {
                timerView.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                timerView.setText("done!");
            }
        }.start();
        timerRunning = true;
    }

    private void stopTimer() {
        timer.cancel();
        timerRunning = false;
    }

    public void onClickAddTime(View view) {
        countdownTime += 30000;
        if(timerRunning == false) timerView.setText(Long.toString(countdownTime / 1000));
    }

    public void onClickSubtractTime(View view) {
        if(countdownTime >= 30000) countdownTime -= 30000;
        if(timerRunning == false) timerView.setText(Long.toString(countdownTime / 1000));

    }

    public void onClickReset(View view) {
        if(timerRunning) stopTimer();
        timerView.setText(Long.toString(countdownTime / 1000));
    }

    public void onClickAddSet(View view) {
        currSetNum++;
        liftSetCounter.setText("Current Set #: " + currSetNum);
    }

    public void onClickResetSetCount(View view) {
        currSetNum = 0;
        liftSetCounter.setText("Current Set #: " + currSetNum);
    }
}